<section class="logo-1" id="<?php the_sub_field("id_sekcji"); ?>">
	<div class="container">
		<div class="row">
			<h2>Nasi partnerzy:</h2>
		</div>
		<div class="row">
			<?php if( have_rows('logo') ):  while ( have_rows('logo') ) : the_row();  ?>
			<div class="col-xl-2 col-md-3 wow fadeInRight">
				<div class="logo-box">
					<?php echo wp_get_attachment_image( get_sub_field('grafika'), "kontener", "", array( "class" => "lazy, img-fluid", "data-src=" => $grafika ) );  ?>
				</div>
			</div>
			<?php endwhile; else : endif; ?>
		</div>
	</div>
</section>
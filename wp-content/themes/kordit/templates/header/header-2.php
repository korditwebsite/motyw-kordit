<nav id="mainmenu">
	<?php $custom_logo_id = get_theme_mod( 'custom_logo' );
	$logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
	if ( has_custom_logo() ) {
		echo '<img alt="logotyp" class="img-fluid" src="'. esc_url( $logo[0] ) .'">';
	} else {
		echo '<h1>'. get_bloginfo( 'name' ) .'</h1>';
	} ?>
	<?php
	wp_nav_menu([
		'menu'            => 'top',
		'theme_location'  => 'top',
		'container'       => 'div',
		'container_class' => 'nawigacja-menu',
		'menu_class'      => 'navbar-nav',
		'depth'           => 2,
		'fallback_cb'     => 'bs4navwalker::fallback',
		'walker'          => new bs4navwalker(),
	]);
	?>
</nav>
<div class="menu-button-2" id="menu-button">
	<button class="button-toggle-navigation">
		<span>Toggle Navigation</span>
	</button>
</div>
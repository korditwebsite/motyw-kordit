<div class="top-header">
  <div class="container">
    <div class="row">
      <ul>
        <li><img src="/wp-content/themes/kordit/img/call.png"><a href="tel:+48693190036">693 190 036</a>
        </li>
        <li><img src="/wp-content/themes/kordit/img/mail.png"><a href="mailto:stol-frez@wp.pl">stol-frez@wp.pl</a></li>
      </ul>
    </div>
  </div>
</div>
<nav class="navbar navbar-expand-lg navbar-dark scrolling-navbar">
  <div class="container">
    <a class="navbar-brand" href="/">
      <div class="brand-logo">
        <?php $custom_logo_id = get_theme_mod( 'custom_logo' );
        $logo = wp_get_attachment_image_src( $custom_logo_id , 'headerlogo' );
        if ( has_custom_logo() ) {
          echo '<img class="img-fluid" src="'. esc_url( $logo[0] ) .'">';
        } else {
          echo '<h1>'. get_bloginfo( 'name' ) .'</h1>';
        } ?>
      </div>
    </a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-7" aria-controls="navbarSupportedContent-7" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent-7">
      <?php
      wp_nav_menu([
        'menu'            => 'top',
        'theme_location'  => 'top',
        'container'       => 'div',
        'container_class' => '',
        'menu_id'         => false,
        'menu_class'      => 'navbar-nav',
        'depth'           => 2,
        'fallback_cb'     => 'bs4navwalker::fallback',
        'walker'          => new bs4navwalker()
      ]);
      ?>
    </div>
  </div>
</nav>
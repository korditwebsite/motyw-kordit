<section id="<?php the_sub_field("id_sekcji"); ?>">
	<div id="paralaxoption" style="background-image: url(<?php echo wp_get_attachment_image_url( get_sub_field('tlo'), "hero_image" ); ?>);">
		<div class="container wowparalax">
			<div class="row wow fadeInUp">

				<h2><?php the_sub_field("tytul"); ?></h2>
				<p><?php the_sub_field("opis"); ?></p>
			</div>
			<div class="row">
				<?php if( have_rows('wypunktowanie') ): $x = 0; while ( have_rows('wypunktowanie') ) : the_row(); ?>
					<div class="col-xl-4 col-md-4 col-12">
						<div class="item">
							<div class="thumbnail">
								<?php $namesvg = "paralax-svg-" . $x ?>
								<div id="<?php echo $namesvg; ?>">
									<?php echo file_get_contents( wp_get_attachment_image_url( get_sub_field('grafika'))); ?>
								</div>
							</div>
							<h3><?php the_sub_field('tytul'); ?></h3>
							<p><?php the_sub_field('opis'); ?></p>
						</div>
					</div>
					<?php $x = $x +1; endwhile; else : endif; ?>
				</div>
			</div>
		</div>
	</section>
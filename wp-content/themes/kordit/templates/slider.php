<?php
$licznikslajdow = 0;
$slajd = 0;
?>
<section id="<?php the_sub_field("id_naglowka"); ?>">
  <?php if( have_rows('slider-content') ): ?>
    <div id="slider-carousel" class="carousel slide" data-ride="carousel">

      <ol class="carousel-indicators">
        <?php $x = 0; while ( have_rows('slider-content') ) : the_row();
        if ($x <= 0) {
          $active ="active";
        } else {
          $active = " ";
        }
        ?>
        <li data-target="#slider" data-slide-to="<?php echo $x ?>" class="<?php echo $active ?>"></li>

        <?php $x = $x + 1; endwhile; ?>
      </ol>
      <div class="carousel-inner" role="listbox">
        <!--First slide-->
        <?php $x = 0; while ( have_rows('slider-content') ) : the_row();
        if ($x <= 0) {
          $active ="active";
        } else {
          $active = " ";
        }
        ?>

        <div class="carousel-item <?php echo $active ?>">
          <div class="view">
            <?php $grafika = wp_get_attachment_image_url( get_sub_field('grafika'), "kontener" ); ?>
            <?php echo wp_get_attachment_image( get_sub_field('grafika'), "hero_image", "", array( "class" => "lazy", "data-src=" => $grafika ) );  ?>
            <div class="mask rgba-black-light"></div>
          </div>

          <div class="carousel-caption">
            <h3>
              <?php the_sub_field('naglowek'); ?>
            </h3>
            <h6>
              <?php the_sub_field('podpis'); ?>
            </h6>
            <?php if( have_rows('przycisk') ): while( have_rows('przycisk') ): the_row(); ?>
              <a href="<?php the_sub_field('link'); ?>">
               <button >
                <?php the_sub_field('napis'); ?>
              </button>
            </a>
          </div>
        <?php endwhile; endif; ?>
      </div>

      <?php $x = $x + 1; endwhile; ?>
      <!--/Third slide-->
    </div>

    <a class="carousel-control-prev" href="#slider-carousel" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>

    <a class="carousel-control-next" href="#slider-carousel" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
  <?php  else : endif; ?>
</section>
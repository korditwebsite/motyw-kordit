<section class="gallery gallery-2">
	<div class="container-fluid">
		<div class="row">
			<h2><?php the_sub_field("tytul"); ?></h2>
			<p><?php the_sub_field("opis"); ?></p>
			<?php $images = get_sub_field('galeria');
			$size = 'gallery';
			$size2 = 'hero_image';
			if( $images ): ?>

				<?php $time = 0; foreach( $images as $image ): ?>
				<figure class="col-xl-3 col-md-3 col-6">
					<div class="row">
						<?php 
						$wielkosc = getimagesize(wp_get_attachment_image_url( $image['ID'],$size2));
						$height = $wielkosc[0];
						$width = $wielkosc[1];
						?>
						<a class="wow fadeInUp js-smartPhoto" class="js-smartPhoto" data-caption="<?php echo $image['description'] ?>" href="<?php echo wp_get_attachment_image_url( $image['ID'], $size2 ); ?>">
							<img class="lazy" alt="item-gallery" src="<?php echo wp_get_attachment_image_url( $image['ID'], $size ); ?>" src="<?php echo wp_get_attachment_image_url( $image['ID'], $size ); ?>" class="img-fluid">
						</a>
					</div>
				</figure>
				<?php $time = $time +250; endforeach; ?>
			<?php endif;  ?>
		</div>
	</div>
</section>


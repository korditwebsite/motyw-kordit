<section class="kontakt kontakt-2" id="<?php the_sub_field("id_sekcji"); ?>" style="background-image: url(<?php echo wp_get_attachment_image_url( get_sub_field('tlo'), "hero_image" ); ?>);">
	<div class="container">
		<?php
		$mobile = "/wp-content/themes/kordit/img/mail.svg";
		?>
		<div class="row wowparalax">
			<div class="col-xl-3">
				<div class="inner-opis">
					<h2><?php the_sub_field("tytul"); ?></h2>
					<div class="phone wow">
						<?php if( have_rows('numery_telefonow') ): while ( have_rows('numery_telefonow') ) : the_row(); ?>
							<img src="/wp-content/themes/kordit/img/call-2.png">
						<a href="<?php the_sub_field('numer_tel'); ?>"><?php the_sub_field('numer_tel'); ?></a>
						<?php endwhile; else : endif; ?>
					</div>
					<div class="mail wow">
						<?php if( have_rows('adres_e-mail') ): while ( have_rows('adres_e-mail') ) : the_row(); ?>
							<img src="/wp-content/themes/kordit/img/mail-2.png">
						<a href="<?php the_sub_field('numer_tel'); ?>"><?php the_sub_field('numer_tel'); ?></a>
						<?php endwhile; else : endif; ?>
					</div>
					<div class="home wow">
						<div class="text">
							<img src="/wp-content/themes/kordit/img/icon-2.png">
							<?php the_sub_field('dodatkowe_informacje'); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-xl-9">
				<div class="contact-box">
					<?php echo do_shortcode('[contact-form-7 id="5" title="Formularz 1"]'); ?>
				</div>
			</div>
		</div>
	</div>
</section>
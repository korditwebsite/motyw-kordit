<section class="service-1" id="<?php the_sub_field("id_sekcji"); ?>">
	<div class="container">
		<div class="row">
			<div class="col-xl-6 col-md-6 col-12">
				<div class="thumbnail responsive">
					<?php echo wp_get_attachment_image( get_sub_field('grafika'), "o-nas", "", array( "class" => "js-smartPhoto") );  ?>
				</div>
			</div>
			<div class="col-xl-6 col-md-6 col-12">
				<h2><?php the_sub_field("tytul"); ?></h2>
				<p><?php the_sub_field("tresc"); ?></p>
			</div>
		</div>
	</div>
</section>


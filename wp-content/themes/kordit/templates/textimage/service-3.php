<section class="service-3" id="<?php the_sub_field("id_sekcji"); ?>">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-6 col-md-12 col-12 wow fadeInLeft">
				<div class="thumbnail responsive">
					<?php $grafika = wp_get_attachment_image_url( get_sub_field('grafika'), "kontener" ); ?>
					<?php echo wp_get_attachment_image( get_sub_field('grafika'), "kontener", "", array( "class" => "lazy", "data-src=" => $grafika ) );  ?>
				</div>
			</div>
			<div class="col-xl-6 col-md-12 col-12 wow fadeInRight">
				<div class="content">
					<h2><?php the_sub_field("tytul"); ?></h2>
					<p><?php the_sub_field("tresc"); ?></p>
				</div>
			</div>
		</div>
	</div>
</section>
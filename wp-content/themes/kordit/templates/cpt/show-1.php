<section class="cpt-1" id="<?php the_sub_field("id_sekcji"); ?>" style="background-image: url(<?php echo wp_get_attachment_image_url( get_sub_field('tlo'), "hero_image" ); ?>);">
	<div class="container">
		<div class="row">	
			<?php
			$nazwaKategori = "realizacje";
			$cat = ( isset( $_GET['wyroznione'] ) ) ? $_GET['wyroznione'] : 1;
			$args = array(
				'post_type'   => 'oferta',
				'post_status' => 'publish',
				'order' => 'ASC',
				'posts_per_page'=>'4',
			);

			$testimonials = new WP_Query( $args ); 
		// echo "<pre>"; print_r($testimonials);
			if( $testimonials->have_posts() ) :
				?>
				<?php
				while( $testimonials->have_posts() ) :
					$testimonials->the_post();
					?>
					<div class="col-xl-3 col-lg-4 col-12">
						<div class="item-portfolio wow fadeInUp">
							<div class="thumbnail">
								<?php the_post_thumbnail('home-thumbnail', array('class' => 'lazy')); ?>
							</div>
							<h5><?php printf(get_the_title());  ?></h5>
							<p><?php the_field("krotki_opis"); ?></p>
						</div>
					</div>
					<?php
				endwhile;
				wp_reset_postdata();
				?>
				<?php
			else :
				esc_html_e( 'Kategoria w trakcie uzupełniania, zapraszamy wkrótce!', 'text-domain' );
			endif;
			?>
		</div>
	</div>
</section>


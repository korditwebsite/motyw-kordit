<section class="oferta-1" id="<?php the_sub_field("id_sekcji"); ?>">
	<div class="container">
		<div class="row">
			<?php if( have_rows('oferta_pojedyncza') ):  while ( have_rows('oferta_pojedyncza') ) : the_row();  ?>
			<div class="col-xl-4 col-md-4 wow fadeInRight offer-wrapper">
				<div class="offer-box">
					<?php echo wp_get_attachment_image( get_sub_field('grafika'), "kontener", "", array( "class" => "lazy, img-fluid", "data-src=" => $grafika ) );  ?>
					<h3><?php the_sub_field("tytul"); ?></h3>
					<p><?php the_sub_field("tresc"); ?></p>
				</div>
			</div>
			<?php endwhile; else : endif; ?>
		</div>
	</div>
</section>
<section class="phone-box" id="<?php the_sub_field("id_sekcji"); ?>">
	<div class="container">
		<div class="row">
			<div class="col-xl-6 col-md-6 col-12">
				<div class="phone-box-text">
					<?php the_sub_field("tresc"); ?>
				</div>
			</div>
			<div class="col-xl-6 col-md-6 col-12">
				<a href="tel:<?php the_sub_field("telefon"); ?>"><?php the_sub_field("telefon"); ?></a>
			</div>
		</div>
	</div>
</section>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<?php wp_head(); ?>
	<title><?php wp_title('the_title_attribute();'); ?></title>
</head>
<body <?php body_class( 'class-name' ); ?>>
	<?php 
	$wyborheadera = get_field('wybor_headera', 'option');
	?>
	<header id="header-<?php echo $wyborheadera; ?>">
		<?php get_template_part( 'templates/header/header', $wyborheadera);?>
	</header>
	





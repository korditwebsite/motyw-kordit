var $ = jQuery;
//Nawigacja 
$("#menu-button").click(function(){
	$("nav").toggleClass("active");
});
$("a").click(function(){
	$("nav").removeClass("active");
	$("#menu-button").removeClass("open");
});


//Nawigacja header 1
$(".menu-button-1").click(function(){
	$("main").toggleClass("blur");
});
$("a").click(function(){
	$("main").removeClass("blur");
});

//Nawigacja header 2
$(".menu-button-2").click(function(){
	$("main").toggleClass("blur-1");
});
$("a").click(function(){
	$("main").removeClass("blur-1");
});

// //inicjacja galerii 
// $(function () {
// 	$("#mdb-lightbox-ui").load("/wp-content/themes/icomweb/assets/mdb-addons/mdb-lightbox-ui.html");
// });
$(document).ready(function() {
	
//usuwanie przycisków, gdy content pusty. Pomaga przy sliderze
$('button').filter(function() {
	return $(this).html().trim().length == 0
}).remove();

//aktywacja nawigacji
$('#menu-button button').on('click', function() {
	$(this).toggleClass('isActive');
});
});
window.addEventListener('DOMContentLoaded',function(){
	new SmartPhoto(".js-smartPhoto");
});


var iteracja = 0; 

$( document ).ready(function() {

	var wow = new WOW(
	{
    boxClass:     'wowparalax',      // animated element css class (default is wow)
    animateClass: 'animated', // animation css class (default is animated)
    offset:       0,          // distance to the element when triggering the animation (default is 0)
    mobile:       true,       // trigger animations on mobile devices (default is true)
    live:         true,       // act on asynchronously loaded content (default is true)
    callback:     function(box) {
    	do {
    		var svganimacja = "#paralax-svg-" + iteracja;
    		var svg = new Walkway({
    			selector: svganimacja + ' svg',
    			easing: 'easeInOutCubic',
    			duration: 5100
    		}).draw();
    		iteracja++;
    	}
    	while (iteracja < 10);
    },
    scrollContainer: null // optional scroll container selector, otherwise use window
}
);
	wow.init();
});
new WOW().init();






//lazy load
jQuery(document).ready(function() {

    jQuery(".lazy").lazy({
        effect          : "fadeIn",   // this only works on images !
        effectTime      : 1000,
        combined        : true,
        delay           : 12500,
        scrollDirection : 'vertical',
        visibleOnly     : false,      // could be removed, it's default value
        threshold       : 2000,
        defaultImage    : "",         // this is a bug, that the default image is set to non-images
                                      // i'll updated this within the next version
                                      afterLoad: function(element)
                                      {
                                        element.removeClass("loading").addClass("loaded");
                                    }
                                });
});
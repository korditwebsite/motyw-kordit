<?php 
$wyborfootera = get_field('wybor_footera', 'option');
?>
<footer id="footer-<?php echo $wyborfootera; ?>">
	<?php get_template_part( 'templates/footer/footer', $wyborfootera);?>
</footer>
<?php wp_footer(); ?>
</body>
</html>
<?php get_header(); ?>
<main id="realizacje">
	<canvas id="linie" width="1550" height="400"></canvas>
	<section id="portfolio" class="panel" data-section-name="portfolio">
		<div class="container">
			<h1 class="title"><?php the_title(); ?></h1>
			<a class="wroc" href="/portfolio">Wróć do wszystkich realizacji</a>
			<article>
				<div class="tab-content">
					<div class="contain row">
						<div class="row">
							<div class="col-xl-6">
								<div class="thumbnail wow fadeInLeft">
									<?php the_post_thumbnail( 'big' ); ?>
								</div>
							</div>
							<div class="col-xl-6">
								<div class="short-text wow fadeInRight">
									<?php the_field("krotki_opis"); ?>
									<?php if( have_rows('przycisk') ): while( have_rows('przycisk') ): the_row(); ?>
										<a target="_blank" href="<?php the_sub_field("link"); ?>"><button>zobacz na żywo</button></a>
									<?php endwhile; endif; ?>
								</div>
							</div>
							<div class="col-xl-12">
								<h3 class="title mt-5">Galeria realizacji</h3>

								<div id="mdb-lightbox-ui">

								</div>
								<div class="mdb-lightbox" data-pswp-uid="1">
									<div class="row">
										<?php $images = get_field('galeria');
										$size = 'gallery';
										$size2 = 'hero_image';
										if( $images ): ?>
											<?php $time = 0; foreach( $images as $image ): ?>
											<figure class="col-xl-3 col-md-3 col-6">
												<?php 
												$wielkosc = getimagesize(wp_get_attachment_image_url( $image['ID'],$size2));
												$height = $wielkosc[0];
												$width = $wielkosc[1];
												?>

												<a class="wow fadeInUp" data-wow-delay="<?php echo $time; ?>ms" href="<?php echo wp_get_attachment_image_url( $image['ID'], $size2 ); ?>" data-size="<?php echo $height;?>x<?php echo $width;?>">
													<img alt="item-gallery" src="<?php echo wp_get_attachment_image_url( $image['ID'], $size ); ?>" class="img-fluid z-depth-1">
												</a>
											</figure>
											<?php $time = $time +250; endforeach; ?>
										<?php endif;  ?>
									</div>
								</div>
							</div>
							<div class="col-xl-12">

							</div>
						</div>
					</div>
				</div>
			</article>
		</div>
		<div class="container">
			<div class="looks">
				<div class="row">
					<?php $number = get_field("numer"); ?>
					<h3 class="title text-center mt-5 mb-3">Sprawdź podobne realizacje</h3>
					<?php
					$nazwaKategori = "realizacje";
					$cat = ( isset( $_GET['wyroznione'] ) ) ? $_GET['wyroznione'] : 1;
					$args = array(
						'post_type'   => 'realizacja',
						'post_status' => 'publish',
						'order' => 'ASC',
						'posts_per_page'=>'3',
						'tax_query' => [
							[

								'taxonomy' => 'rodzaj',
								'field' => 'term_id',
								'terms' => $number,
							]
						],
					);

					$testimonials = new WP_Query( $args ); 
		// echo "<pre>"; print_r($testimonials);
					if( $testimonials->have_posts() ) :
						?>
						<?php
						while( $testimonials->have_posts() ) :
							$testimonials->the_post();
							?>
							<div class="col-lg-4 col-12">
								<div class="item-portfolio">
									<?php
									if ( has_post_thumbnail()  ) {
										the_post_thumbnail( 'home-thumbnail' );
									}
									?>
									<a href="<?php the_permalink(); ?>"><button>Sprawdź realizacje</button></a>
									<h5><?php printf(get_the_title());  ?></h5>
								</div>
							</div>
							<?php
						endwhile;
						wp_reset_postdata();
						?>
						<?php
					else :
						esc_html_e( 'Kategoria w trakcie uzupełniania, zapraszamy wkrótce!', 'text-domain' );
					endif;
					?>
				</div>
				<div class="fly-letter">
					<span><?php the_title(); ?></span>
				</div>
				<div id="cta">
					<div class="container mt-5 pb-5">
						<div class="row">
							<div class="col-xl-8 offset-xl-2">
								<h3 class="title">Chciałbyś podobną realizację?</h3>
								<?php if( have_rows('kontakt', 'option') ): while( have_rows('kontakt', 'option') ): the_row(); ?>
									<div class="buttons">
										<a href="tel:<?php the_sub_field('numer_telefonu'); ?>">
											<button>Zadzwoń <?php the_sub_field('numer_telefonu'); ?></button>
										</a>
										<a href="<?php the_field("link"); ?>">
											<button>przejdź do oferty</button>
										</a>
									</div>
								<?php endwhile; endif; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</main>
	<?php get_footer(); ?>
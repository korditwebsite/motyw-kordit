<?php
/**
* Template Name: Strona główna
*/
?>
<?php get_header(); ?>
<main>
	<?php get_header(); 
// check if the flexible content field has rows of data
	if( have_rows('pojedyncza_sekcja') ):

     // loop through the rows of data
		while ( have_rows('pojedyncza_sekcja') ) : the_row();


//slider
			if( get_row_layout() == 'slider' ): 
				get_template_part( 'templates/slider'); 

			//grafika z opisem
			elseif( get_row_layout() == 'grafika_z_opisem' ):
				$numersekcji = get_sub_field("ulozenie");
				get_template_part( 'templates/textimage/service', $numersekcji);     

				//paralax
			elseif( get_row_layout() == 'paralax' ):
				get_template_part( 'templates/paralax');     

						//galeria
			elseif( get_row_layout() == 'galeria' ):
				$numersekcji = get_sub_field("ulozenie");
				get_template_part( 'templates/gallery/galeria', $numersekcji);         

				//cpt
			elseif( get_row_layout() == 'prezentacja_cpt' ):
				$numersekcji = get_sub_field("ulozenie");
				get_template_part( 'templates/cpt/show', $numersekcji);     

//kontakt
			elseif( get_row_layout() == 'kontakt' ):
				$numersekcji = get_sub_field("ulozenie");
				get_template_part( 'templates/kontakt/kontakt', $numersekcji);   

			endif;

		endwhile;

	else :

    // no layouts found

	endif;

	?>
</main>
<?php get_footer(); ?> 
